<%@page import="java.util.ArrayList"%>
<%@page import="CatalogueDesProduits.Article"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Article</title>
</head>
<body>
	<form method="POST">
		<h2>Ajouter un article</h2>
		<p>Designation de l'article :</p>
		<input type="text" name="nom"> <br />
		<p>Prix de l'article:</p>
		<input type="text" name="prix" /> <input type="submit" value="Submit"
			name="creer" />
	</form>

	<form method="POST">
		<h2>Effacer un article</h2>
		<p>ID de l'article a enlever :</p>
		<input type="text" name="idArticle"> <input type="submit"
			value="Submit" name="delete" />
	</form>

	<p>
		<%
			if (request.getAttribute("confirmation") != null)  
				out.println((String) request.getAttribute("confirmation"));
		%>
	</p>

	<form method="POST">
		<h2>Afficher les articles</h2>
		<input type="submit" name="afficheListe" value="Afficher la Liste"
			onclick="recupererListe()" />
	</form>

	<div id="divArticle">
		<%
			if (request.getAttribute("listeArticle") != null) {
				ArrayList<Article> liste = (ArrayList) request.getAttribute("listeArticle");
				for (int i = 0; i < liste.size(); i++) {
					out.println("<p>" + liste.get(i) + "</p>");
				}
			}
		%>
	</div>

</body>
</html>
