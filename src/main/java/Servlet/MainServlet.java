package Servlet;

import java.io.*;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import CatalogueDesProduits.Article;
import Utilitaire.RequeteDAO;
import Utilitaire.Validation;

public class MainServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private RequeteDAO requestDAO;
	private HttpSession session;
	private String username;

	public MainServlet() {
		super();
		requestDAO = RequeteDAO.getInstance();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getParameter("authentification") != null)
			authentification(request, response);
		if (request.getParameter("afficheListe") != null)
			afficheListe(request, response);
		if (request.getParameter("creer") != null)
			creer(request, response);
		if (request.getParameter("delete") != null)
			delete(request, response);
		loadPage(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		loadPage(request, response);
	}

	private void loadPage(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (username == null) {
			this.getServletContext().getRequestDispatcher("/WEB-INF/authentification.jsp").forward(request, response);
		} else {
			this.getServletContext().getRequestDispatcher("/WEB-INF/article.jsp").forward(request, response);
		}
	}

	private void afficheListe(HttpServletRequest request, HttpServletResponse response) {
		ArrayList<Article> listeArticle = null;
		listeArticle = (ArrayList<Article>) requestDAO.lireArticles();
		request.setAttribute("listeArticle", listeArticle);
	}

	private void creer(HttpServletRequest request, HttpServletResponse response) {
		String nom = request.getParameter("nom");
		String prix = request.getParameter("prix");
		if (Validation.valideString(nom)) {
			try {
				Article article = new Article(nom, Double.parseDouble(prix));
				requestDAO.ajouterArticle(article);
				request.setAttribute("confirmation", "Article ajouté!");
			} catch (NumberFormatException e) {
				request.setAttribute("confirmation", "Le prix doit être un nombre.");
			}
		} else {
			request.setAttribute("confirmation", "Tout les champ doit être remplit.");
		}
	}

	private void delete(HttpServletRequest request, HttpServletResponse response) {
		String ID = request.getParameter("idArticle");
		if (Validation.valideString(ID) && ID != "") {
			requestDAO.deleteArticle(ID);
			request.setAttribute("confirmation", "Article effacé!");
		} else
			request.setAttribute("confirmation", "ID de l'article est invalide.");
	}

	private void authentification(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String login = request.getParameter("login");
		String mdp = request.getParameter("mdp");
		if (session == null)
			session = request.getSession(true);
		if (Validation.valideString(login) && Validation.valideString(mdp)) {
			if (requestDAO.authentification(login, mdp)) {
				session.setAttribute("username", login);
				username = login;
			} else {
				request.setAttribute("confirmation", "Utilisateur ou mot de passe invalide.");
			}
		} else {
			request.setAttribute("confirmation", "Utilisateur ou mot de passe invalide.");
		}
	}
}
