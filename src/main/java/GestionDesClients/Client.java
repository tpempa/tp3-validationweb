package GestionDesClients;

import java.util.HashMap;

import DonneesTechniques.Personne;
import GestionDesVentes.Commande;

public class Client extends Personne {

	private HashMap<String, Commande> commandes = new HashMap<>();

	public Client(String nom, String prenom) {
		super(nom, prenom);
	}

	private double updateSolde() {
		double total = 0;
		for (String ID : commandes.keySet()) {
			total += commandes.get(ID).getSoldeAPayer();
		}
		return total;
	}

	public void addCommande(Commande commande) {
		commandes.put(commande.getIDCommande(), commande);
	}

	public Commande getCommande(String ID) {
		return commandes.get(ID);
	}

	public HashMap<String, Commande> getAllCommandes() {
		return commandes;
	}

	public double getSoldeCourant() {
		return updateSolde();
	}

}
