package Utilitaire;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import CatalogueDesProduits.Article;
import DonneesTechniques.Commis;
import DonneesTechniques.Gestionnaire;
import DonneesTechniques.Technicien;
import DonneesTechniques.Utilisateur;

public class XML {

	public static ArrayList<Utilisateur> readUserXML(URL url) {
		DocumentBuilderFactory docBF = DocumentBuilderFactory.newInstance();
		DocumentBuilder docB;
		Document doc = null;
		try {
			docB = docBF.newDocumentBuilder();
			doc = docB.parse(new File(url.toURI()));
		} catch (ParserConfigurationException | SAXException | IOException | URISyntaxException e) {
			e.printStackTrace();
		}

		Element utilisateurs = (Element) doc.getElementsByTagName("utilisateurs").item(0);
		NodeList nodeUser = utilisateurs.getElementsByTagName("user");

		ArrayList<Utilisateur> listUser = new ArrayList<>();
		for (int i = 0; i < nodeUser.getLength(); i++) {
			Element node = (Element) nodeUser.item(i);
			Utilisateur user = null;
			String classe = node.getAttribute("class");
			if ("DonneesTechniques.Commis".equals(classe)) {
				user = new Commis(node.getAttribute("name"), node.getAttribute("prenom"),
						node.getAttribute("password"));
			}
			if ("DonneesTechniques.Gestionnaire".equals(classe)) {
				user = new Gestionnaire(node.getAttribute("name"), node.getAttribute("prenom"),
						node.getAttribute("password"));
			}
			if ("DonneesTechniques.Technicien".equals(classe)) {
				user = new Technicien(node.getAttribute("name"), node.getAttribute("prenom"),
						node.getAttribute("password"));
			}
			listUser.add(user);
		}
		return listUser;
	}

	public static ArrayList<Article> readArticleXML(URL url) {

		DocumentBuilderFactory docBF = DocumentBuilderFactory.newInstance();
		DocumentBuilder docB;
		Document doc = null;
		try {
			docB = docBF.newDocumentBuilder();
			doc = docB.parse(new File(url.toURI()));
		} catch (ParserConfigurationException | SAXException | IOException | URISyntaxException e) {
			e.printStackTrace();
		}

		Element test = (Element) doc.getElementsByTagName("dataset").item(0);
		NodeList nodeArticle = test.getElementsByTagName("article");

		ArrayList<Article> listArticle = new ArrayList<>();
		for (int i = 0; i < nodeArticle.getLength(); i++) {
			Element node = (Element) nodeArticle.item(i);
			Article article = new Article();
			article.setIDArticle(node.getAttribute("IDArticle"));
			article.setDesignationArticle(node.getAttribute("designationArticle"));
			article.setPrixDeVenteSuggere(Double.parseDouble(node.getAttribute("prixDeVenteSuggere")));

			listArticle.add(article);
		}
		return listArticle;
	}

	public static void saveArticleXML(ArrayList<Article> listArticle, URL url) {
		DocumentBuilderFactory docBF = DocumentBuilderFactory.newInstance();
		DocumentBuilder docB = null;
		try {
			docB = docBF.newDocumentBuilder();
		} catch (ParserConfigurationException e1) {
			e1.printStackTrace();
		}
		Document doc = docB.newDocument();

		Element dataset = doc.createElement("dataset");
		doc.appendChild(dataset);

		for (int i = 0; i < listArticle.size(); i++) {
			Article artcile = listArticle.get(i);

			Element elem = doc.createElement("article");

			elem.setAttribute("IDArticle", artcile.getIDArticle());
			elem.setAttribute("designationArticle", artcile.getDesignationArticle());
			elem.setAttribute("prixDeVenteSuggere", String.valueOf(artcile.getPrixDeVenteSuggere()));

			dataset.appendChild(elem);
		}

		TransformerFactory transFac = TransformerFactory.newInstance();
		Transformer trans = null;
		try {
			trans = transFac.newTransformer();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		}
		DOMSource source = new DOMSource(doc);

		StreamResult streamResult = null;

		try {
			streamResult = new StreamResult(new File(url.toURI()));
			trans.transform(source, streamResult);
		} catch (TransformerException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		try {
			System.out.println(url.toURI());
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws Exception {
		System.out.println("allo");
		DocumentBuilderFactory docBF = DocumentBuilderFactory.newInstance();
		DocumentBuilder docB = docBF.newDocumentBuilder();
		Document doc = docB.newDocument();

		Element element = doc.createElement("Developer");
		doc.appendChild(element);

		Attr attr = doc.createAttribute("Id");
		attr.setValue("1");
		element.setAttributeNode(attr);

		Element name = doc.createElement("Name");
		name.appendChild(doc.createTextNode("Sahil"));
		element.appendChild(name);

		Element surname = doc.createElement("Surname");
		surname.appendChild(doc.createTextNode("Huseynzade"));
		element.appendChild(surname);

		Element age = doc.createElement("Age");
		age.appendChild(doc.createTextNode("2"));
		element.appendChild(age);

		TransformerFactory transFac = TransformerFactory.newInstance();
		Transformer trans = transFac.newTransformer();
		DOMSource source = new DOMSource(doc);

		StreamResult streamResult = new StreamResult(new File("U:\\test\\data.xml"));

		trans.transform(source, streamResult);

	}
}