package Utilitaire;

public class Validation {
	private static final int MAX_STRING_SIZE = 30;

	public static boolean valideString(String nom) {
		return (nom.length() <= MAX_STRING_SIZE && nom != null && nom != "");
	}

	public static boolean valideMotDePasse(String motDePasse) {
		if (motDePasse.length() < 8 || motDePasse.length() > 15)
			return false;
		int nombreChiffre = 0;
		for (char character : motDePasse.toCharArray()) {
			try {
				Integer.parseInt(String.valueOf(character));
				nombreChiffre++;
			} catch (NumberFormatException ex) {

			}
		}
		if (nombreChiffre < 2)
			return false;
		return true;
	}
}
