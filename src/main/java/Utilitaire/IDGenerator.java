package Utilitaire;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;

public class IDGenerator {

	private static LinkedHashMap<Integer, Integer> incrementMAP = new LinkedHashMap<>();

	public static String genereID(Object object) {
		String id = "";
		String indexe = String.valueOf(object.getClass().getSimpleName().charAt(0));
		String increment = "00000";
		int annee = LocalDateTime.now().getYear();

		if (incrementMAP.containsKey(LocalDateTime.now().getYear())) {
			increment = String.valueOf(incrementMAP.get(annee));
			incrementMAP.put(annee, Integer.valueOf(increment) + 1);
		} else {
			incrementMAP.put(annee, 10000);
			increment = String.valueOf(incrementMAP.get(annee));
			incrementMAP.put(annee, Integer.valueOf(increment) + 1);
		}

		id = indexe + LocalDateTime.now().getYear() + increment;

		return id;
	}

}
