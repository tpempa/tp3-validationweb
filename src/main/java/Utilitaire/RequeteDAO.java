package Utilitaire;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import CatalogueDesProduits.Article;
import DonneesTechniques.Commis;
import DonneesTechniques.Gestionnaire;
import DonneesTechniques.Technicien;
import DonneesTechniques.Utilisateur;

public class RequeteDAO {
	static RequeteDAO dao = null;
	static Connection con = null;

	private static final String DRIVER = "org.hsqldb.jdbcDriver";
	private static final String URL = "jdbc:hsqldb:mem:mymemdb";
	private static final String LOGIN = "sa";
	private static final String MDP = "";
	private static final String RESOURCE_ARTICLE = "/article_xml.xml";
	private static final String RESOURCE_USER = "/user_xml.xml";

	public static RequeteDAO getInstance(String driver, String url, String login, String mdp) {
		if (dao == null) {
			dao = new RequeteDAO(driver, url, login, mdp);
		}
		return dao;
	}

	public static RequeteDAO getInstance() {
		if (dao == null) {
			dao = new RequeteDAO(DRIVER, URL, LOGIN, MDP);
		}
		return dao;
	}

	private RequeteDAO(String driver, String url, String login, String mdp) {
		try {
			Class.forName(driver).newInstance();
			con = DriverManager.getConnection(url, login, mdp);
			executeRequest("DROP SCHEMA PUBLIC CASCADE");
			executeRequest(
					"CREATE TABLE article (IDArticle varchar(30), designationArticle varchar(30), prixDeVenteSuggere double, PRIMARY KEY (IDArticle))");
			executeRequest(
					"CREATE TABLE user (name varchar(30),prenom varchar(30), password varchar(30), class varchar(30), PRIMARY KEY (name))");
			loadXML();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	private void loadXML() {

		ArrayList<Article> listeArticle = XML.readArticleXML(this.getClass().getResource(RESOURCE_ARTICLE));
		for (int i = 0; i < listeArticle.size(); i++) {
			ajouterArticle(listeArticle.get(i));
		}

		ArrayList<Utilisateur> listeUser = XML.readUserXML(this.getClass().getResource(RESOURCE_USER));
		for (int i = 0; i < listeUser.size(); i++) {
			ajouteUtilisateur(listeUser.get(i));
		}
	}

	public void ajouteUtilisateur(Utilisateur user) {
		String request = "INSERT INTO user values(";
		request += "'" + user.getNom() + "'" + ", ";
		request += "'" + user.getPrenom() + "'" + ", ";
		request += "'" + user.getMotDePasse() + "'" + ", ";
		request += "'" + user.getClass().getName() + "'" + ")";
		executeRequest(request);
	}

	public void saveXML() {
		XML.saveArticleXML(lireArticles(), getClass().getResource(RESOURCE_ARTICLE));
	}

	public ArrayList<Utilisateur> lireUtilisateurs() {
		ResultSet result = executeRequest("SELECT name, prenom, password, class FROM user");
		ArrayList<Utilisateur> liste = new ArrayList<Utilisateur>();
		try {
			while (result.next()) {
				Utilisateur user = null;
				String classe = result.getString(4);
				String nom = result.getString(1);
				String prenom = result.getString(2);
				String pass = result.getString(3);

				if (classe.equals("DonneesTechniques.Commis")) {
					user = new Commis(nom, prenom, pass);
				} else if (classe.equals("DonneesTechniques.Gestionnaire")) {
					user = new Gestionnaire(nom, prenom, pass);
				} else if (classe.equals("DonneesTechniques.Technicien")) {
					user = new Technicien(nom, prenom, pass);
				}

				liste.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return liste;
	}

	public void ajouterArticle(Article article) {
		String request = "INSERT INTO article values(";
		request += "'" + article.getIDArticle().toString() + "'" + ", ";
		request += "'" + article.getDesignationArticle().toString() + "'" + ", ";
		request += article.getPrixDeVenteSuggere() + ")";
		executeRequest(request);
		// saveXML();
	}

	public void deleteArticle(String IDArticle) {
		String request = "DELETE FROM article WHERE IDArticle = ";
		request += "'" + IDArticle + "'";
		executeRequest(request);
		// saveXML();
	}

	public ArrayList<Article> lireArticles() {
		ResultSet result = executeRequest("SELECT IDArticle, designationArticle, prixDeVenteSuggere FROM article");
		ArrayList<Article> liste = new ArrayList<Article>();
		try {
			while (result.next()) {
				Article article = new Article();
				article.setIDArticle(result.getString(1));
				article.setDesignationArticle(result.getString(2));
				article.setPrixDeVenteSuggere(result.getDouble(3));
				liste.add(article);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return liste;
	}

	private ResultSet executeRequest(String request) {
		ResultSet result = null;
		try {
			System.err.println(request);
			result = con.createStatement().executeQuery(request);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean authentification(String login, String mdp) {
		ArrayList<Utilisateur> user = lireUtilisateurs();
		for (int i = 0; i < user.size(); i++) {
			if (login.equals(user.get(i).getNom())) {
				if (mdp.equals(user.get(i).getMotDePasse())) {
					return true;
				}
			}
		}
		return false;
	}

	public static void main(String[] args) {
		RequeteDAO dao = getInstance();

		System.out.println(dao.lireArticles());
		System.out.println(dao.lireUtilisateurs());
	}
}
