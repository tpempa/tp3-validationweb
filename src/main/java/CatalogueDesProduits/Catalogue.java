package CatalogueDesProduits;

import java.util.Date;
import java.util.HashSet;

import Utilitaire.IDGenerator;
import Utilitaire.Validation;

public class Catalogue {

	private Date dateProduction = null;
	private String IDCode;
	private HashSet<Article> articles = new HashSet<Article>();
	private String designationCatalogue;

	public Catalogue(String designationCatalogue, Date dateProduction) {
		
		assert (designationCatalogue.length()<=30 && designationCatalogue.length()>=1 && designationCatalogue != null && designationCatalogue != "") : "designationCatalogue non valide";
		
		if (setDesignationCatalogue(designationCatalogue) && setDateProduction(dateProduction)) {
			setIDCode(IDGenerator.genereID(this));
		}
	}

	public Date getDateProduction() {
		return dateProduction;
	}

	public boolean setDateProduction(Date dateProduction) {
		this.dateProduction = dateProduction;
		return true;
	}

	public String getIDCode() {
		return IDCode;
	}

	private void setIDCode(String iDCode) {
		IDCode = iDCode;
	}

	public HashSet<Article> getArticles() {
		return articles;
	}

	public String getDesignationCatalogue() {
		return designationCatalogue;
	}

	private boolean setDesignationCatalogue(String designationCatalogue) {
		if (Validation.valideString(designationCatalogue)) {
			this.designationCatalogue = designationCatalogue;
			return true;
		}
		return false;
	}
	
	public void addArticle(Article article) {
		this.articles.add(article);
	}

}
