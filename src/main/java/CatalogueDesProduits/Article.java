package CatalogueDesProduits;

import Utilitaire.IDGenerator;
import Utilitaire.Validation;

public class Article {

	private String IDArticle = "Invalide";
	private String designationArticle = "Invalide";
	private double prixDeVenteSuggere = 0;

	public Article() {
	}

	public Article(String designation, double prixDeVenteSuggere) {

		assert (designation.length() <= 30 && designation.length() >= 1 && designation != null
				&& designation != "") : "designation non valide";

		if (setDesignationArticle(designation) && setPrixDeVenteSuggere(prixDeVenteSuggere)) {
			setIDArticle(IDGenerator.genereID(this));
		}
	}

	public String getDesignationArticle() {
		return designationArticle;
	}

	// exemple un marteau
	public boolean setDesignationArticle(String designationArticle) {
		if (Validation.valideString(designationArticle)) {
			this.designationArticle = designationArticle;
			return true;
		}
		return false;
	}

	public double getPrixDeVenteSuggere() {
		return prixDeVenteSuggere;
	}

	public boolean setPrixDeVenteSuggere(double prixDeVenteSuggere) {
		this.prixDeVenteSuggere = prixDeVenteSuggere;
		return true;
	}

	public void setIDArticle(String iDArticle) {
		IDArticle = iDArticle;
	}

	public String getIDArticle() {
		return IDArticle;
	}

	public static Object getListArticles() {
		return null;
	}

	@Override
	public String toString() {
		return "IDArticle : " + IDArticle + " Designation Article : " + designationArticle + " Prix de Vente Suggere : "
				+ prixDeVenteSuggere;
	}

}
