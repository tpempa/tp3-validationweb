package DonneesTechniques;

import DonneesTechniques.Utilisateur;

public class Technicien extends Utilisateur {

	public Technicien(String nom, String prenom, String motDePasse) {
		super(nom, prenom, motDePasse);
		changeAcces(TypeAcces.Technicien);
	}
}
