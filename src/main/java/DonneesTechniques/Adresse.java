package DonneesTechniques;

import Utilitaire.Validation;

public class Adresse {

	private String numeroCivique = null;
	private String rue = null;
	private String ville = null;
	private String codePostal = null;

	public Adresse(String noCivique, String rue, String ville, String codePostal) {
		
		assert (noCivique.length()<=30 && noCivique.length()>=1 && noCivique != null && noCivique != "") : "noCivique non valide";
		assert (rue.length()<=30 && rue.length()>=1 && rue != null && rue != "") : "rue non valide";
		assert (codePostal.length()<=30 && codePostal.length()>=1 && codePostal != null && codePostal != "") : "codePostal non valide";
		assert (ville.length()<=30 && ville.length()>=1 && ville != null && ville != "") : "ville non valide";
		
		setNumeroCivique(noCivique);
		setCodePostal(codePostal);
		setRue(rue);
		setVille(ville);
	}

	public String getNumeroCivique() {
		return numeroCivique;
	}

	private boolean setNumeroCivique(String numeroCivique) {
		if (Validation.valideString(numeroCivique)) {
			this.numeroCivique = numeroCivique;
			return true;
		}
		return false;
	}

	public String getRue() {
		return rue;
	}

	private boolean setRue(String rue) {
		if (Validation.valideString(rue)) {
			this.rue = rue;
			return true;
		}
		return false;
	}

	public String getVille() {
		return ville;
	}

	private boolean setVille(String ville) {
		if (Validation.valideString(ville)) {
			this.ville = ville;
			return true;
		}
		return false;
	}

	public String getCodePostal() {
		return codePostal;
	}

	private boolean setCodePostal(String codePostal) {
		if (Validation.valideString(codePostal)) {
			this.codePostal = codePostal;
			return true;
		}
		return false;
	}
}
