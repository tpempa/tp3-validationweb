package DonneesTechniques;

import java.util.Date;

import Utilitaire.Validation;

public class DetailProbleme {

	private String descriptionProbleme = null;
	private Date dateSignaler = null;

	public DetailProbleme(Date date, String description) {
		
		assert (description.length()<=30 && description.length()>=1 && description != null && description != "") : "motDePasse non valide";
		
		
		setDescriptionProbleme(description);
		setDateSignaler(date);
	}

	public Date getDateSignaler() {
		return dateSignaler;
	}

	public String getDescriptionProbleme() {
		return descriptionProbleme;
	}

	public boolean setDescriptionProbleme(String descriptionProbleme) {
		if (Validation.valideString(descriptionProbleme)) {
			this.descriptionProbleme = descriptionProbleme;
			return true;
		}
		return false;
	}

	public void setDateSignaler(Date dateSignaler) {
		this.dateSignaler = dateSignaler;
	}
}
