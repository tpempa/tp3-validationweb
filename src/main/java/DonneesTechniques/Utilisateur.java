package DonneesTechniques;

import DonneesTechniques.Personne;
import DonneesTechniques.TypeAcces;
import DonneesTechniques.Utilisateur;
import Utilitaire.Validation;

public abstract class Utilisateur extends Personne {

	private String motDePasse = null;
	private TypeAcces typeAcces;

	public Utilisateur(String nom, String prenom, String motDePasse) {
		super(nom, prenom);

		assert (motDePasse.length() <= 30 && motDePasse.length() >= 1 && motDePasse != null
				&& motDePasse != "") : "motDePasse non valide";
		setMotDepasse(motDePasse);
	}

	private boolean setMotDepasse(String motDePasse) {
		if (Validation.valideMotDePasse(motDePasse)) {
			this.motDePasse = motDePasse;
			return true;
		}
		return false;
	}

	public String getMotDePasse() {
		return motDePasse;
	}

	public TypeAcces getTypeAcces() {
		return typeAcces;
	}

	protected void changeAcces(TypeAcces typeAcces) {
		this.typeAcces = typeAcces;
	}

	public static boolean valideConnection(String login, String motDePasse) {
		return true;
	}

	@Override
	public String toString() {
		return "Utilisateur motDePasse : " + motDePasse + ", Nom : " + getNom() + ", Prenom : " + getPrenom();
	}
}
