package DonneesTechniques;

import java.util.HashSet;

import Utilitaire.IDGenerator;

public class Probleme {

	public HashSet<DetailProbleme> detailProblemes = new HashSet<>();
	public HashSet<Utilisateur> utilisateurs = new HashSet<Utilisateur>();
	private String IDProbleme;
	public TypeProbleme typeProblemes;

	public Probleme(TypeProbleme type) {
		typeProblemes = type;
		setIDProbleme(IDGenerator.genereID(this));
	}

	public void setIDProbleme(String string) {
		IDProbleme = string;
	}

	public String getIDProbleme() {
		return IDProbleme;
	}

	public TypeProbleme getTypeProbleme() {
		return typeProblemes;
	}

	public void addDetailProbleme(DetailProbleme detail) {
		detailProblemes.add(detail);
	}

	public HashSet<DetailProbleme> getDetailProbleme() {
		return detailProblemes;
	}

}
