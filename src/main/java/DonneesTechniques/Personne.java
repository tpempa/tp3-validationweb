package DonneesTechniques;

import DonneesTechniques.Personne;
import Utilitaire.IDGenerator;
import Utilitaire.Validation;

public abstract class Personne {

	private Adresse adresses = null;
	private String ID = null;
	private String prenomPersonne = null;
	private String nomPersonne = null;

	public Personne(String nom, String prenom) {
		assert (nom.length()<=30 && nom.length()>=1 && nom != null && nom != "") : "nom non valide";
		assert (prenom.length()<=30 && prenom.length()>=1 && prenom != null && prenom != "") : "prenom non valide";
		
		if (setNomPersonne(nom) && setPrenomPersonne(prenom)) {
			setID(IDGenerator.genereID(this));
		}
	}

	private boolean setPrenomPersonne(String prenom) {
		if (Validation.valideString(prenom)) {
			prenomPersonne = prenom;
			return true;
		} else {
			return false;
		}
	}

	private boolean setNomPersonne(String nom) {
		if (Validation.valideString(nom)) {
			nomPersonne = nom;
			return true;
		} else {
			return false;
		}
	}

	private void setID(String id) {
		ID = id;
	}

	public String getNom() {
		return nomPersonne;
	}

	public String getPrenom() {
		return prenomPersonne;
	}

	public String getID() {
		return ID;
	}

	public Adresse getAdresses() {
		return adresses;
	}

	public void setAdresses(Adresse adresse) {
		adresses = adresse;
	}
}
