package DonneesTechniques;

import DonneesTechniques.Utilisateur;

public class Gestionnaire extends Utilisateur {
	
	public Gestionnaire(String nom, String prenom, String motDePasse) {
		super(nom, prenom, motDePasse);
		changeAcces(TypeAcces.Gestionnaire);
	}
}