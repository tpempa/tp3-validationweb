package DonneesTechniques;

import DonneesTechniques.Utilisateur;

public class Commis extends Utilisateur {
	public Commis(String nom, String prenom, String motDePasse) {
		super(nom, prenom, motDePasse);
		changeAcces(TypeAcces.Commis);
	}
}
