package GestionDesVentes;

import java.util.Date;

import Utilitaire.IDGenerator;
import Utilitaire.Validation;

public class Paiement {

	private String IDPaiement = null;
	private Date datePaiement = null;
	private double montantPaiement = 0;
	private String modePaiement = null;

	public Paiement(Date datePaiement, double montantPaiement, String modePaiement) {
		
		assert (modePaiement.length()<=30 && modePaiement.length()>=1 && modePaiement != null && modePaiement != "") : "modePaiement non valide";
		
		setIDPaiement(IDGenerator.genereID(this));
		setMontantPaiement(montantPaiement);
		setModePaiement(modePaiement);
		setDatePaiement(datePaiement);
	}

	public String getIDPaiement() {
		return IDPaiement;
	}

	private void setIDPaiement(String iDPaiement) {
		IDPaiement = iDPaiement;
	}

	public Date getDatePaiement() {
		return datePaiement;
	}

	public void setDatePaiement(Date datePaiement) {
		this.datePaiement = datePaiement;
	}

	public double getMontantPaiement() {
		return montantPaiement;
	}

	public void setMontantPaiement(double montantPaiement) {
		this.montantPaiement = montantPaiement;
	}

	public String getModePaiement() {
		return modePaiement;
	}

	public boolean setModePaiement(String modePaiement) {
		if (Validation.valideString(modePaiement)) {
			this.modePaiement = modePaiement;
			return true;
		}
		return false;
	}
}
