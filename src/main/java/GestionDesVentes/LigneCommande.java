
package GestionDesVentes;

import CatalogueDesProduits.Article;

public class LigneCommande {

	private double prixDeVente;
	public Article article = null;
	private int quantite = 0;

	public LigneCommande(Article article, int quantite) {
		setArticle(article);
		setQuantite(quantite);
	}

	private void updatePrixVente() {
		setPrixDeVente(quantite * article.getPrixDeVenteSuggere());
	}

	public double getPrixDeVente() {
		return prixDeVente;
	}

	private void setPrixDeVente(double prixDeVente) {
		this.prixDeVente = prixDeVente;
	}

	public Article getArticles() {
		return article;
	}

	private void setArticle(Article article) {
		this.article = article;
		updatePrixVente();
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
		updatePrixVente();
	}
}
