package GestionDesVentes;

import GestionDesVentes.LigneCommande;
import Utilitaire.IDGenerator;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

public class Commande {

	private String IDCommande;
	public HashSet<LigneCommande> ligneCommandes = new HashSet<LigneCommande>();
	public ArrayList<Paiement> lisPaiment = new ArrayList<>();
	private Date dateLivraison = null;
	private Date datePassationCommande = null;
	private Double prixTotal = Double.valueOf(0D);

	public Commande(Date dateLivraison, Date datePassationCommande) {
		setIDCommande(IDGenerator.genereID(this));
		setDateLivraison(dateLivraison);
		setDatePassationCommande(datePassationCommande);

	}

	private void updatePrixTotal() {
		Double prix = 0.00;
		for (LigneCommande ligneCommande : ligneCommandes) {
			prix += ligneCommande.getPrixDeVente();
		}
		this.prixTotal = prix;
	}

	public void addLigneCommande(LigneCommande ligneCommande) {
		ligneCommandes.add(ligneCommande);
		updatePrixTotal();
	}

	public String getIDCommande() {
		return IDCommande;
	}

	private void setIDCommande(String iDCommande) {
		IDCommande = iDCommande;
	}

	public HashSet<LigneCommande> getLigneCommandes() {
		return ligneCommandes;
	}

	public Date getDateLivraison() {
		return dateLivraison;
	}

	private void setDateLivraison(Date dateLivraison) {
		this.dateLivraison = dateLivraison;
	}

	public Date getDatePassationCommande() {
		return datePassationCommande;
	}

	private void setDatePassationCommande(Date datePassationCommande) {
		this.datePassationCommande = datePassationCommande;
	}

	public double getMontantTotal() {
		return prixTotal;
	}

	public void setMontantTotal(Double montantTotal) {
		this.prixTotal = montantTotal;
	}

	public double getSoldeAPayer() {
		double total = 0;
		for (Paiement paiement : lisPaiment) {
			total += paiement.getMontantPaiement();
		}
		return (prixTotal - total);
	}

	public void addPaiement(Paiement paimenent) {
		lisPaiment.add(paimenent);
		updatePrixTotal();
	}
}
