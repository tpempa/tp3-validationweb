package testDBUnit;

import java.util.List;

import org.dbunit.DBTestCase;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;

import CatalogueDesProduits.Article;
import DonneesTechniques.Gestionnaire;
import DonneesTechniques.Utilisateur;
import Utilitaire.RequeteDAO;
import junit.framework.Assert;

public class RequeteDAOTest extends DBTestCase {

	private static final String DRIVER = "org.hsqldb.jdbcDriver";
	private static final String URL = "jdbc:hsqldb:mem:mymemdb";
	private static final String LOGIN = "sa";
	private static final String MDP = "";
	private static Boolean uneSeuleFois = true;
	RequeteDAO dao;
	ITable referenceDataSetTable, resultatTable;

	@Override
	public void setUp() throws Exception {
		super.setUp();
		dao = RequeteDAO.getInstance(DRIVER, URL, LOGIN, MDP);
	}

	public RequeteDAOTest(String name) throws Exception {
		super(name);
		System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS, DRIVER);
		System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL, URL);
		System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME, LOGIN);
		System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD, MDP);

		if (uneSeuleFois) {
			uneSeuleFois = false;
			String r = "CREATE TABLE article (IDArticle varchar(30), designationArticle varchar(30), prixDeVenteSuggere double, PRIMARY KEY (IDArticle))";
			getConnection().getConnection().createStatement().execute("DROP SCHEMA PUBLIC CASCADE");

			getConnection().getConnection().createStatement().execute(r);
			getConnection().getConnection().createStatement().execute(
					"CREATE TABLE user (name varchar(30),prenom varchar(30), password varchar(30), class varchar(30), PRIMARY KEY (name))");
		}
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSetBuilder().build(this.getClass().getResourceAsStream("/RequeteDAOTest_getDataSet.xml"));
	}

	@Override
	protected DatabaseOperation getSetUpOperation() throws Exception {
		return DatabaseOperation.CLEAN_INSERT;
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	public void testLireArticle() throws Exception {
		List<Article> liste = dao.lireArticles();
		Assert.assertEquals(2, liste.size());

		Article article1 = liste.get(0);
		assertTrue(article1.getIDArticle().equals("testID"));
		assertTrue(article1.getDesignationArticle().equals("Marteau"));
		assertTrue(String.valueOf(article1.getPrixDeVenteSuggere()).equals("10.0"));

		Article article2 = liste.get(1);
		assertTrue(article2.getIDArticle().equals("testID2001"));
		assertTrue(article2.getDesignationArticle().equals("Bateau"));
		assertTrue(String.valueOf(article2.getPrixDeVenteSuggere()).equals("2000.55"));

	}

	public void testAjouterArticle() throws Exception {

		Article article = new Article();
		article.setIDArticle("testID205");
		article.setDesignationArticle("Tournevis");
		article.setPrixDeVenteSuggere(15.0);
		dao.ajouterArticle(article);

		assertTrue(dao.lireArticles().get(2).getIDArticle().equals("testID205"));
		assertTrue(dao.lireArticles().get(2).getDesignationArticle().equals("Tournevis"));
		assertTrue(String.valueOf(dao.lireArticles().get(2).getPrixDeVenteSuggere()).equals("15.0"));

		Article article2 = new Article();
		dao.ajouterArticle(article2);

		assertTrue(dao.lireArticles().get(0).getIDArticle().equals("Invalide"));
		assertTrue(dao.lireArticles().get(0).getDesignationArticle().equals("Invalide"));
		assertTrue(String.valueOf(dao.lireArticles().get(0).getPrixDeVenteSuggere()).equals("0.0"));
	}

	public void testDeleteArticle() throws Exception {

		int sizeInitial = dao.lireArticles().size();

		// test avec un ID erroner
		dao.deleteArticle("fafdsfafvwe");
		assertTrue((dao.lireArticles().size()) == sizeInitial);

		// test avec un ID existant
		dao.deleteArticle("testID");
		assertTrue((dao.lireArticles().size() + 1) == sizeInitial);

		// test avec un ID existant
		dao.deleteArticle("testID2001");
		assertTrue((dao.lireArticles().size() + 2) == sizeInitial);
	}

	public void testlireUtilisateurs() throws Exception {

		assertTrue(dao.lireUtilisateurs().get(1).getPrenom().equals("istrateur"));
		assertTrue(dao.lireUtilisateurs().get(1).getNom().equals("admin"));
		assertTrue(dao.lireUtilisateurs().get(1).getMotDePasse().equals("Admin12345"));
	}

	public void testAjouteUtilisateur() throws Exception {

		Utilisateur utilisateur = new Gestionnaire("Dion", "Pascal", "MotDepas12");
		dao.ajouteUtilisateur(utilisateur);

		assertTrue(dao.lireUtilisateurs().get(0).getPrenom().equals("Pascal"));
		assertTrue(dao.lireUtilisateurs().get(0).getNom().equals("Dion"));
		assertTrue(dao.lireUtilisateurs().get(0).getMotDePasse().equals("MotDepas12"));
	}

	public void testAuthentification() throws Exception {

		assertFalse(dao.authentification("", ""));
		assertFalse(dao.authentification("eeewwe", "Adsdf332"));
		assertFalse(dao.authentification("admin", "Adsdf332"));
		assertTrue(dao.authentification("admin", "Admin12345"));
	}
}
