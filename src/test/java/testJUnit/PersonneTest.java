package testJUnit;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import DonneesTechniques.Adresse;
import DonneesTechniques.Commis;
import DonneesTechniques.Gestionnaire;
import DonneesTechniques.Technicien;
import DonneesTechniques.Utilisateur;

class PersonneTest {

	Utilisateur commis;
	Utilisateur technicien;
	Utilisateur gestionnaire;
	Adresse adresse;
	
	@BeforeEach
	void setUp() throws Exception {
		commis = new Commis("commisNom","commisPrenom","qwerty12345");
		technicien = new Technicien("techNom", "techPrenom", "qwerty12345");
		gestionnaire = new Gestionnaire("gestionnaireNom", "gestionnairePrenom", "qwerty12345");
		
		adresse = new Adresse("90", "bob rue", "Quebec", "777 777");
		commis.setAdresses(adresse);
	}

	@Test
	void testGetNom() {
		assertEquals("commisNom", commis.getNom());
		assertEquals("techNom", technicien.getNom());
		assertEquals("gestionnaireNom", gestionnaire.getNom());
	}

	@Test
	void testGetPrenom() {
		assertEquals("commisPrenom", commis.getPrenom());
		assertEquals("techPrenom", technicien.getPrenom());
		assertEquals("gestionnairePrenom", gestionnaire.getPrenom());
	}

	@Test
	void testGetID() {
		assertTrue((commis.getID()!=null && commis.getID().length()==10));
		assertTrue((technicien.getID()!=null && technicien.getID().length()==10));
		assertTrue((gestionnaire.getID()!=null && gestionnaire.getID().length()==10));
	}

	@Test
	void testGetAdresses() {
		assertEquals(adresse, commis.getAdresses());
	}

}
