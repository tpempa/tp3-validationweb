package testJUnit;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import CatalogueDesProduits.Article;

class ArticleTest {

	Article article;

	@BeforeEach
	void setUp() throws Exception {
		article = new Article("marteau", 10.00);
	}

	@Test
	void testGetDesignationArticle() {
		assertEquals("marteau", article.getDesignationArticle());
	}

	@Test
	void testGetPrixDeVenteSuggere() {
		assertEquals(10.00, article.getPrixDeVenteSuggere());
	}

}
