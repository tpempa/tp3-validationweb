package testJUnit;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import DonneesTechniques.Adresse;

class AdresseTest {

	Adresse adresse1;

	@BeforeEach
	void setUp() throws Exception {
		adresse1 = new Adresse("9000", "Boulevard Roger", "Quebec", "G1G5G5");
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void getNumeroCivique() {
		assertEquals("9000", adresse1.getNumeroCivique());
	}
	
	@Test
	void getRue() {
		assertEquals("Boulevard Roger", adresse1.getRue());
	}
	
	@Test
	void getVille() {
		assertEquals("Quebec", adresse1.getVille());
	}
	
	@Test
	void getCodePostal() {
		assertEquals("G1G5G5", adresse1.getCodePostal());
	}

}
