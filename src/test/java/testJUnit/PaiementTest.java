package testJUnit;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import GestionDesVentes.Paiement;

class PaiementTest {
	
	Paiement paiement;
	Date date;
	@BeforeEach
	void setUp() throws Exception {
		date = new Date();
		paiement = new Paiement(date, 5.0, "masterVisa");
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	

	@Test
	void testGetIDPaiement() {
		assertTrue((paiement.getIDPaiement()!=null && paiement.getIDPaiement().length()==10));
	}

	@Test
	void testGetDatePaiement() {
		assertEquals(date, paiement.getDatePaiement());
	}


	@Test
	void testGetMontantPaiement() {
		assertEquals(5.0, paiement.getMontantPaiement());
	}


	@Test
	void testGetModePaiement() {
		assertEquals("masterVisa", paiement.getModePaiement());
	}


}
