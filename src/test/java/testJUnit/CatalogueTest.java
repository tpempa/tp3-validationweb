package testJUnit;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import CatalogueDesProduits.Article;
import CatalogueDesProduits.Catalogue;

class CatalogueTest {
	
	Catalogue catalogue;
	Article article;
	Date date;
	@BeforeEach
	void setUp() throws Exception {
		article = new Article("marteau", 10.00);
		date = new Date();
		catalogue = new Catalogue("Outils", date);
		catalogue.addArticle(article);
	}

	@AfterEach
	void tearDown() throws Exception {
		catalogue.getArticles().clear();
	}


	

	@Test
	void testGetDateProduction() {
		assertEquals(date, catalogue.getDateProduction());
	}

	@Test
	void testGetIDCode() {
		assertTrue((catalogue.getIDCode()!=null && catalogue.getIDCode().length()==10));
	}

	

	@Test
	void testGetArticles() {
		assertTrue(catalogue.getArticles().size()>0);
	}

	@Test
	void testGetDesignationCatalogue() {
		assertEquals("Outils", catalogue.getDesignationCatalogue());
	}

}
