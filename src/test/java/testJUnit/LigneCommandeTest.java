package testJUnit;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import CatalogueDesProduits.Article;
import GestionDesVentes.LigneCommande;

class LigneCommandeTest {
	LigneCommande ligne;
	Article article;
	@BeforeEach
	void setUp() throws Exception {
		article = new Article("marteau", 10.00);
		ligne = new LigneCommande(article, 5);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testGetPrixDeVente() {
		assertEquals(50.00, ligne.getPrixDeVente());
	}

	@Test
	void testGetArticles() {
		assertEquals(article, ligne.getArticles());
	}

}
