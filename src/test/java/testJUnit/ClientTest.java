package testJUnit;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import CatalogueDesProduits.Article;
import GestionDesClients.Client;
import GestionDesVentes.Commande;
import GestionDesVentes.LigneCommande;
import GestionDesVentes.Paiement;

class ClientTest {
	LigneCommande ligne1,ligne2,ligne3;
	Article article1,article2,article3;
	Commande commande;
	Date date;
	Client client;
	@BeforeEach
	void setUp() throws Exception {
		date = new Date();
		article1 = new Article("marteau", 10.00);
		article2 = new Article("clou", 0.05);
		article3 = new Article("couteau", 500.00);
		ligne1 = new LigneCommande(article1, 5);
		ligne2 = new LigneCommande(article2, 50);
		ligne3 = new LigneCommande(article3, 500);
		commande = new Commande(date, date);
		commande.addLigneCommande(ligne1);
		commande.addLigneCommande(ligne2);
		commande.addLigneCommande(ligne3);
		commande.addPaiement(new Paiement(date, 200, "visaMaster"));
		client = new Client("clientNom", "clientPrenom");
		client.addCommande(commande);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testGetCommande(){
		assertEquals(commande, client.getCommande(commande.getIDCommande()));
	}

	@Test
	void testGetSoldeCourant() {
		assertEquals(249852.5, client.getSoldeCourant());
	}

}
