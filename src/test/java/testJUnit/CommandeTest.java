package testJUnit;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import CatalogueDesProduits.Article;
import GestionDesVentes.Commande;
import GestionDesVentes.LigneCommande;
import GestionDesVentes.Paiement;

class CommandeTest {
	LigneCommande ligne1,ligne2,ligne3;
	Article article1,article2,article3;
	Commande commande;
	Date date;
	@BeforeEach
	void setUp() throws Exception {
		date = new Date();
		article1 = new Article("marteau", 10.00);
		article2 = new Article("clou", 0.05);
		article3 = new Article("couteau", 500.00);
		ligne1 = new LigneCommande(article1, 5);
		ligne2 = new LigneCommande(article2, 50);
		ligne3 = new LigneCommande(article3, 500);
		commande = new Commande(date, date);
		commande.addLigneCommande(ligne1);
		commande.addLigneCommande(ligne2);
		commande.addLigneCommande(ligne3);
		commande.addPaiement(new Paiement(date, 200, "visaMaster"));
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	

	@Test
	void testGetIDCommande() {
		assertTrue((commande.getIDCommande()!=null && commande.getIDCommande().length()==10));
	}

	@Test
	void testGetLigneCommandes() {
		assertTrue(commande.getLigneCommandes().contains(ligne1));
	}

	@Test
	void testGetDateLivraison() {
		assertEquals(date, commande.getDateLivraison());
	}

	@Test
	void testGetDatePassationCommande() {
		assertEquals(date, commande.getDatePassationCommande());
	}

	@Test
	void testGetMontantTotal() {
		assertEquals(250052.5 , commande.getMontantTotal());
	}

	

	@Test
	void testGetSoldeAPayer() {
		assertEquals(249852.5 , commande.getSoldeAPayer());
	}

	

}
